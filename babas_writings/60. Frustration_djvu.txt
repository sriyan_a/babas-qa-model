FRUSTRATION 


The Untouchables are the weariest, most loathed and the most miserable 
people that history can witness. They area spent and sacrificed people. To 
use the language of Shelley they are — 

" pale for weariness of climbing heaven, and gazing on earth, 

wandering companionless Among the stars that have a different birth " 

To put it in simple language the Untouchables have been completely 
overtaken by a sense of utter frustration. As Mathew Arnold says "life consists 
in the effort to affirm one's own essence ; meaning by this, to develop one's 
own existence fully and freely, to have ample light and air, to be neither (. . . . 

. . .) nor overshadowed. Failure to affirm ones own essence is simply another 
name for frustration. Its non fulfilment of one's efforts to do the best, the 
withering of one's faculties, the stunting of one's personality. " 

Many people suffer such frustrations in their history. But they soon recover 
from the blight and rise to glory again with new vibrations. The case of the 
Untouchables stands on a different footing. Their frustration is frustration for 
ever. It is unrelieved by space or time. 

In this respect the story of the Untouchables stands in strange contrast 
with that of the Jews. 

Their captivity in Egypt was the first calamity that visited the Jewish 
people. As the Bible says 

[Quote Childem's Bible-39] (Quotation not recorded — ed.) 

Ultimately Pharaoh yielded. The Jewish people escaped captivity and 
went to Cannan and settled thee in the land flowing with milk and honey. 

The second calamity which overtook the Jews was the Babylonian 
Captivity. (Some pages are missing — ed.) 

We can now explain why the Untouchables have suffered frustration. They 
have no plus condition of body and mind. They have nothing in their dull drab 
deadening past for a hope of a rise in the future to feed upon. This is due to 
no fault of theirs. The frustration which is their fate is the result of the 
unpropitious social environment born out of the Hindu Social Order which is 
so deadly inimical to their progress. 

Their fate is entirely unbearable. As Carlyle has said — 

[Quote p. 201] 
(Quotation not cited — ed.) 

Some are thinking of revolutions, even bloody revolutions to overthrow the 
Hindu Social Order. All are saying what Cabli Williams once said — 

[Quote p. 152 ] 



Such is the degree of frustration they feel 


(Quotation not cited — ed.) 


III 

The Covenant with God may be interpreted to mean in the language of 
Emerson a plus condition of mind and body. As Emerson has said " Success 
is constitutional-depends, on a plus condition of mind and body — on power of 
work — on courage. Success goes invariably with a certain plus or positive 
power: An ounce of Power must balance an ounce of weight." 

If the Jews rose after their first captivity, it was primarily because of their 
plus condition of mind and body. This plus condition of mind and body can 
arise from two sources. It can arise from reliance on God. God, if nothing else 
is at least a source of power and in emergency man needs mental power, the 
plus condition of mind and body which is necessary for success. There is 
therefore nothing wrong in the suggestion that the Jews succeeded because 
of their Covenant of God if it is interpreted in the right way. 

IV 

This plus condition of body and mind is also the result of Social 
Environment, if the Environment is propitious. In a society where there is 
exemption from restraint, a secured release from obstruction, in a society 
where every man is entitled not only to the means of being, but also of well- 
being, where no man is forced to labour so that another may abound in 
luxuries, where no man is deprived of his right to cultivate his faculties and 
powers so that there may be no competition with the favoured, where there is 
emphasis of reward by mento, where there is goodwill towards all, (Further 
portion of this part is erased and not legible — ed.) 

( The above portions are in the handwriting of Dr. Ambedkar. Each part is 
written on a separate sheet — ed.). 



