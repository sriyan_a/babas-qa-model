THE PROBLEM OF THE RUPEE: 

ITS ORIGIN AND ITS SOLUTION 

(HISTORY OF INDIAN CURRENCY & BANKING) 


CHAPTER III 

THE SILVER STANDARD AND THE EVILS OF ITS INSTABILITY 

The economic consequences of this rupture of the par of exchange were of 
the most far-reaching character. It divided the commercial world into two 
sharply defined groups, one using gold and the other using silver as their 
standard money. When so much gold was always equal to so much silver, as 
was the case previous to 1873, it mattered very little, for the purposes of 
international transactions, whether a country was on a gold or on a silver 
standard ; nor did it make any difference in which of the two currencies its 
obligations were stipulated and realised. But when, owing to the dislocation of 
the fixed par, it was not possible to define how much silver was equal to how 
much gold from year to year or even from month to month, this precision of 
value, the very soul of pecuniary exchange, gave place to the uncertainties of 
gambling. Of course, all countries were not drawn into this vortex of perplexities 
in the same degree and to the same extent, yet it was impossible for any 
country which participated in international commerce to escape from being 
dragged into it. This was true of India as it was of no other country. She was a 
silver-standard country intimately bound to a gold-standard country, so that her 
economic and financial life was at the mercy of blind forces operating upon the 
relative values of gold and silver which governed the rupee-sterling exchange. 

The fall increased the burden of those who were under an obligation to make 
gold payments. Amongst such, the most heavily charged was the Government 
of India. Owing to the exigencies of its political constitution, that Government 
has been under the necessity of making certain payments in England to meet : 
(1) interest on debt and on the stock of the guaranteed railway companies ; (2) 
expenses on account of the European troops maintained in India; (3) pensions 
and non-effective allowances payable in England; (4) cost of the home 
administration; and (5) stores purchased in England for use or consumption in 
India. England being a gold-standard country, these payments were necessarily 
gold payments. But the revenues of the Government of India out of which these 
payments were met were received in silver, which was the sole legal-tender 
money of the country. It is evident that even if the gold payments were a fixed 




quantity their burden must increase pan passu with the fall in the gold value of 
silver. But the gold payments were not a fixed quantity. They have ever been 
on the increase, so that the rupee cost of the gold payments grew both by 
reason of the growth in their magnitude, and also by reason of the contraction 
of the medium, i.e. the appreciation of gold, in which they were payable. How 
greatly this double levy diminished the revenues of India, the figures in Table XI 
give a convincing testimony. 


TABLE XI 

INCREASE IN THE RUPEE COST OF GOLD PAYMENTS 


Financial 

Year 

Average 
Rate of 
Exchange 
for the 
Year 

Total Excess of 
Rupees needed to 
provide for the net 
Sterling Payments 
of the Year over 
those required to 
meet the Sterling 
Payments of 
1 874-75 

Amount of this Excess due to 




(1) Fall in the Rate 
of Exchange over 
that of 1874-75 

(2) Increase in gold 
payments over 
those of the Year 
1874-75 


s. d. 

R 

R 

R 

1 875-76 

1 9.626 

86,97,980 

41,13,723 

45,84,257 

1 876-77 

1 8.508 

3,15,06,824 

1,44,68,234 

1,70,38,590 

1 877-78 

1 8.791 

1,30,05,481 

1,14,58,670 

1,15,46,811 

1 878-79 

1 7.794 

1,85,23,170 

1,04,16,718 

81,06,452 

1879-80 

1 7.961 

39,23,570 

1,65,37,394 

-1,26,13,824 

1880-81 

1 7.956 

3,12,11,981 

1,92,82,582 

1,19,29,399 

1881-82 

1 7.895 

3,18,19,685 

1,98,76,786 

-1,19,42,899 

1882-83 

1 7.525 

62,50,518 

1,86,35,246 

2,48,85,764 

1883-84 

1 7.536 

3,44,16,685 

2,33,46,040 

1,10,70,645 

1 884-85 

1 7.308 

1,96,25,981 

2,48,03,423 

51 ,77,442 

1885-86 

1 6.254 

1,82,11,346 

2,54,95,337 

-4,37,06,683 

1886-87 

1 5.441 

4,69,16,788 

4,46,68,299 

22,48,489 

1887-88 

1 4.898 

4,63,13,161 

4,96,60,537 

- 33,47,376 

1888-89 

1 4.379 

9,00,38,166 

6,59,71,998 

2,40,66,168 

1889-90 

1 4.566 

7,75,96,889 

6,06,98,370 

1,68,98,519 






1890-91 

1 6.090 

9,06,11,857 

4,65,48,302 

4,40,63,555 

1891-92 

1 4.733 

10,44,44,529 

6,54,52,999 

3,89,91,530 


The effect of such a growing burden on the finance of the Government may 
well be imagined; the condition of the Government, embarrassing at first, later 
became quite desperate under this continuously increasing burden. It enforced 
a policy of high taxation and rigid economy in the finances of the Government. 
Analysing the resource side of the Indian Budgets from the year 1872-73, we 
find that there was hardly any year which did not expire without making an 
addition to the existing imposts of the country. In 1872-73, there commenced 
the levy of what were called Provincial Rates. The fiscal year 1875-76 
witnessed the addition of R. 1 per gallon in the excise duty on spirits. In 1877- 
78 the Pass Duty on Malwa opium was raised from Rs. 600 to Rs. 650 per 
chest. An addition of a License Tax and Local Rates was made in the year 
1878-79, and an increase of Rs. 50 per chest took place in the Malwa Opium 
Duty in the following year. With the help of these imposts the Government 
expected to place its finances on an adequate basis. By the end of 1882, it felt 
quite secure and even went so far as to remit some of the taxes, which it did by 
lowering the customs duties and the Patwari Cess in the North-Western 
Provinces. But the rapid pace in the fall of the exchange soon showed that a 
resort to further taxation was necessary to make up for the increased cost of 
the sterling payments. To the existing burdens, therefore, was added in 1886 
an Income Tax, a duty of 5 per cent, on imported and also on non-illuminating 
petroleum. The Salt Duty was raised in 1888 in India from Rs. 2 to Rs. 2 1/2 
and in Burma from 3 annas to R. 1 per maund. The Patwari Cess of the North- 
Western Provinces, repealed in 1882, was re-imposed in 1888. The rates of 
duty on imported spirit and the excise duties on spirits were not only raised in 
1890, but were afterwards added to in every province. An excise duty on malt 
liquor was levied in 1893, and another on salted fish at the rate of 6 annas per 
maund. The yield of the taxes and duties levied from 1882-83 was as follows: — 


Sources 

1882-83 

1892-93 


Rs. 

Rs. 

Salt 

5,67,50,000 

8,14,90,000 

Excise 

3,47,50,000 

4,97,90,000 

Customs 

1,08,90,000 

1,41,80,000 

Assessed Taxes 

48,40,000 

1,63,60,000 


All this additional burden was due to the enhanced cost of meeting the gold 
payments, and "would not have been necessary but for the fall in the 





exchange." 

Along with this increase of resources the Government of India also exercised 
the virtue of economy in the cost of administration. For the first time in its 
history, the Government turned to the alternative of employing the 
comparatively cheaper agency of the natives of the country in place of the 
imported Englishmen. Prior to 1870, the scope of effecting economy along this 
line was very limited. By the Civil Service Reforms of 1853 the way was cleared 
for the appointment of Indians to the posts reserved by the Statute of 1793 for 
the members of the covenanted Civil Service. But this reform did not conduce 
to any economy in the cost of the administration, because the Indian members 
carried the same high scale of salaries as did the English members of the Civil 
Service. It was when the Statute of 1870 (33 Vic. c. 3) was passed permitting 
the appointment by nomination of non-covenanted Indians to places reserved 
for the covenanted Civil Service on a lower scale of salary, that a real scope for 
economy presented itself to the Government of India. Hard pressed, the 
Government of India availed itself of the possibilities for economy held out by 
this statute. So great was the need for economy and so powerful was the 
interest of the Government in reducing its expenditure that it proceeded, 
notwithstanding increased demands for efficient administration, to substitute the 
less expensive agency of non-covenanted civilians in place of the more 
expensive agency of the covenanted civilians. The scale on which this 
substitution was effected was by no means small, for we find that between 
1874 and 1889 the strength of the covenanted service recruited in England was 
reduced by more than 22 per cent., and was further expected to be reduced by 
about 12 per cent., by the employment of unconvenanted Indians to the posts 
usually reserved for covenanted civilians . Besides substituting a cheap for a 
dear agency in the administration, the Government also sought to obtain relief 
by applying the pruning knife to the rank growth in departmental extravagances. 
Even with such heroic efforts to increase the revenue and reduce the 
expenditure the finances of the Government throughout the period of the falling 
exchange were never in a flourishing state, as is shown in Table XII. 

Much more regrettable was the inability of the Government, owing to its 
financial difficulties, to find money for useful public works. The welfare of the 
Indian people depends upon turning to best account the resources which the 
country possesses. But the people have had very little of the necessary spirit of 
enterprise in them. The task, therefore, has fallen upon the Government of 
India to provide the country with the two prime requisites of a sustained 
economic life, namely a system of transport and a network of irrigation. With 
this object in view the Government had inaugurated a policy of developing what 
were called " Extraordinary Public Works," financed by capital borrowings. For 



such borrowings India, as was to be expected, hardly offered any market, the 
people being too poor and their savings too scanty to furnish a modicum of the 
required capital outlay. Like all governments of poor peoples, the Government 
of India had therefore to turn to wealthier countries that had surplus capital to 
lend. All these countries unfortunately happened to be on the gold standard. As 
long as it was possible to say that so much gold was equal to so much silver, 
the English investor was indifferent whether the securities of the Government of 
India were rupee securities or sterling securities. But the fall in the gold value of 
silver was also a fall in the gold value of the rupee securities, and what was 
once a secure investment ceased to be so any more. This placed the 
Government in a difficult position in the matter of financing its extraordinary 
public works. Figures in Table XIII are worth study. 

The English investor would not invest in the rupee securities. An important 
customer for the Indian rupee securities was thus lost. The response of the 
Indian money market was inadequate. 


TABLE XII 

REVENUE AND EXPENDITURE OF THE GOVERNMENT OF INDIA 


Year. 

Averag 
e Rate 
of 

Exchan 

ge. 

In India. 

In England. 

Final 

Result. 



Net 

Revenue. 

Net 

Expenditure 

excluding 

Exchange. 

Surplus 

Revenue. 

Net Sterling 
Revenue. 

Exchang 

e. 

Surplus 
(+) or 
Deficit ( — 
) 


d. 

R. 

R. 

R. 

£ 

R. 

R. 

1 874-75 

22.156 

39,564,216 

25,897,098 

13,667,118 

12,562,101 

1,045,239 

59,778 

1875-76 

21.626 

40,053,419 

24,541,923 

15,511,496 

12,544,813 

1,377,428. 

1,589,255 

1876-77 

20.508 

38,253,366 

25,355,285 

12,898,081 

13,229,646 

2,252,611 

-2,584,176 

1877-78 

20.791 

39,275,489 

27,658,021 

11,617,468 

13,756,478 

2,123,030 

-4,262,040 

1878-79 

19.794 

44,415,139 

25,778,928 

18,636,211 

13,610,211 

2,891,902 

2,134,098 

1879-80 

19.961 

45,258,197 

29,384,030 

15,874,167 

14,223,891 

2,878,169 

-1,227,893 

1880-81 

19.956 

44,691,119 

34,880,434 

9,810,085 

11,177,231 

2,264,848 

-3,031,394 

1881-82 

19.895 

45,471,887 

27,717,249 

17,754,638 

11,737,688 

2,421,499 

3,595,451 

1882-83 

19.525 

42,526,173 

25,500,437 

17,025,736 

13,299,976 

3,050,923 

674,837 

1883-84 

19.536 

43,591,273 

23,566,381 

20,024,892 

14,770,257 

3,375,158 

1,879,477 

1884-85 

19.308 

41,585,347 

24,763,779 

16,821,568 

13,844,028 

3,363,986 

- 386,446 

1885-86 

18.254 

42,635,953 

27,352,132 

15,283,821 

13,755,659 

4,329,888 

-2,801,726 




1886-87 

17.441 

44,804,774 

25,124,335 

19,680,439 

14,172,298 

5,329,714 

178,427 

1887-88 

16.898 

45,424,150 

25,968,025 

19,456,125 

15,128,018 

6,356,939 

-2,028,832 

1888-89 

16.379 

46,558,354 

25,051,147 

21,507,207 

14,652,590 

6,817,599 

37,018 

1889-90 

16.566 

50,005,810 

26,367,855 

23,637,955 

14,513,155 

6,512,767 

2,612,033 

1890-91 

18.090 

49,403,819 

25,579,727 

23,824,092 

15,176,866 

4,959,055 

3,688,171 

1891-92 

16.733 

50,023,142 

27,013,618 

23,009,524 

15,716,780 

6,825,909 

467,535 


To issue sterling securities was the only alternative to enable the Government 
to tap a bigger and a more constant reservoir for the drawing of capital to India; 
but as it was bound to increase the burden of the gold payments, which it was 
the strongest interest of the Government to reduce, the resort to the London 
money market, unavoidable as it became, was somewhat restrained# with the 
result that the expansion of extraordinary public works did not proceed at a 
pace demanded by the needs of the country. 


#During the period of falling exchange the distribution of the debt of India was as follows: — 



Sterling Debt 

Rupee Debt 

End of 1873-74 

41,117,617 

66,41,72,900 

End of 1898-99 

124,268,605 

1,12,65,04,340 


Indian Currency Committee (1 898), Appendix II p. 179 


The effects of this financial derangement, consequent on the fall of the 

exchange, were not confined to the Government, of India. They were 

immediately felt by the municipalities and other local bodies who were 

dependent upon the Government for financial aid. So long as the cash 

balances were overflowing in the treasury of the Government," one of the most 
useful ways " to employ them was found in lending a portion of them to these 
local institutions. As they had just then been inaugurated under the local self- 
government policy of Lord Ripon's regime, and were looked upon only as an 
experiment, their taxing and borrowing powers were rigidly limited. 
Consequently, this financial aid from the Central Government by way of 
temporary advances was a resource of inestimable value to them. When, 
however, the cash balances of the Central Government began to diminish 
owing to the continued losses by exchange, these facilities were severely 
curtailed, so that the very vitality of these institutions was threatened just at the 
moment when they needed all help to foster their growth and strengthen their 
foundations. 

Addressing the Secretary of State, the Government of India, in a dispatch of 
February 2, 1886, observed — 

" 10. We do not hesitate to repeat that the facts set forth in the preceding 




paragraphs are, from the point of Indian interests, intolerable ; and the evils 
which we have enumerated do not exhaust the catalogue. Uncertainty 
regarding the future of silver discourages the investment of capital in India, and 
we find it impossible to borrow in silver except at an excessive cost. 

"On the other hand, the Frontier and Famine Railways which we propose to 
construct, and the Coast and Frontier defences which we have planned, are 
imperatively required and cannot be postponed indefinitely. 


TABLE XIII 

PRICE MOVEMENTS OF RUPEE AND STERLING SECURITIES OF THE 

GOVERNMENT OF INDIA 


Year. 

Rates of Exchange. 

Price of 4 per cent. Rupee Paper. 

Price of Sterling India Stock. 



In Calcutta. 

In London. 

4 per cent. 

3 1/2 per cent. 

3 per cent. 


Highest. 

Lowest. 

Highest. 

Lowest. 

Highest. 

Lowest. 

Highest. 

Lowest. 

Highest. 

Lowest. 

Highest. 

Lowest. 


d. 

d. 











1873 

22 7/8 

21 5/8 

105 

101 7/8 

97 

941/2 

106 Vi 

101 V* 





1874 

23 1/8 

213 

104 1/2 

99 1/2 

98 

941 

103 3 /4 

101 





1875 

22 3/16 

21 1/4 

102 7/8 

101 3/4 

94 

91 

106 l A 

103 ‘/4 





1876 

22 3/8 

18 1/2 

101 7/8 

98 3/4 

89 3/4 

78 

105 7/8 

101 7/8 





1877 

22 1/4 

20 9/16 

981 

93 1/4 

88 1/2 

81 

104 5/8 

102 3/4 





1878 

21 

18 3/4 

961 

93 1/2 

82 1/2 

751 

104 5/8 

99 





1879 

20 5/8 

18 5/8 

941 

91 1/4 

80 

111 

105 3/8 

100 7/8 





1880 

20 3/8 

19 3/4 

100 

92 15/16 

81 3/8 

77 3/4 

105 3/8 

102 1/8 





1881 

20 1/16 

19 1/2 

104 5/8 

100 

86 

811 

106 3/8 

103 7/8 

103 7/8 

100 3/ 4 



1882 

20 3/16 

19 1/16 

102 1/16 

95 5/8 

85 

81 

105 1/8 

102 7/8 

101 7/8 

99 3/ 4 



1883 

19 9/16 

19 3/16 

101 1/8 

97 9/16 

82 

79 3/4 

104 5/8 

102 7/16 

103 1/8 

101 3/8 



1884 

19 3/4 

18 15/16 

100 5/8 

95 5/16 

811 

78: 

104 3/8 

101 5/8 

107 1/8 

101 3/4 

96 1/4 

91 3/4 

1885 

19 3/16 

17 3/3 1/2 

98 7/16 

921 

77 1/2 

731/4 

103 1/16 

98 3/4 

102 3/4 

97‘/2 

91 1/2 

85 3/4 

1886 

18 

16 1/8 

97 3/4 

97 3/16 

73 

66 l A 

103 Vi 

101 1/4 

102 3/4 

99 3/ 4 

90 1/8 

86 5/8 

1887 

18 3/16 

15 5/8 

99 3/16 

95 5/16 

71 1 1/16 

67 7/8 

102 3 /4 

100 1/2 

103 ‘/4 

100 1/4 

92 3/4 

95 3/8 

1888 

17 1/8 

16 

100 3/16 

971 

691 

66 'A 

102 7/8 

100 1/2 

107 1/4 

104 5/8 

98 

95 

1889 

16 15/16 

16 

100 3/8 

97 1/16 

69 1/8 

66 3/8 



109 1/2 

106 7/8 

101 1/8 

99 

1890 

20 2/3 9/2 

16 7/8 

1031 

96 13/16 

87 'A 

68 3/4 



108 1/2 

105 ‘/4 

100 34 

95 1/4 

1891 

18 1/4 

16 5/8 

107 13/16 

104 1/16 

80 % 

74 Vi 



109 1/2 

105 

99 

94 7/8 

1892 

16 11/16 

14 5/8 

108 15/16 

103 11/16 

74 1/2 

62 



109 ‘/2 

106 1/8 

98‘/2 

94 7/8 


"We are forced, therefore, either to increase our sterling liabilities, to which 
course there are so many objections, or to do without the railways required for 
the commercial development of the country, and its protection against 







invasion and the effects of famine. 


*** 


"11. Nor can the difficulties which local bodies experience in borrowing in 
India be overlooked. The Municipalities of Bombay and Calcutta require large 
sums for sanitary improvements, but the high rate of interest which they must 
pay for silver loans operates to deter them from undertaking expensive works, 
and we need hardly remind your Lordship that it has quite recently been 
found necessary for Government to undertake to lend the money required for 
the construction of docks at Calcutta and Bombay, and that when the Port 
Commissioners of Calcutta attempted to raise a loan of 75 lakhs of rupees in 
September, 1885, guaranteed by the Government of India, the total amount of 
tenders was only Rs. 40,200, and no portion of this insignificant amount was 
offered at par " 

The importation of capital on private account was hampered for similar 
reasons, to the great detriment of the country. It was urged on all hands, and 
was even recommended by a Royal Commission, that one avenue of escape 
from the ravages of recurring famines, to which India so pitifully succumbed at 
such frequent intervals, was the diversification of her industries. To be of any 
permanent benefit, such diversified industrial life could be based on a 
capitalistic basis alone. But that depended upon the flow of capital into the 
country as freely as the needs of the country required. As matters then stood, 
the English investor, the largest purveyor of capital, looked upon the investment 
of capital in India as a risky proposition. It was feared that once the capital was 
spread out in a silver country every fall in the price of silver would not only 
make the return uncertain when drawn in gold, but would also reduce the 
capital value of his investment in terms of gold, which was naturally the unit in 
which he measured all his returns and his outlays. This check to the free inflow 
of capital was undoubtedly the most serious evil arising out of the rupture of the 
par of exchange. 

Another group of people, who suffered from the fall of exchange because of 
their obligation to make gold payments, was composed of the European 
members of the Civil Service in India. Like the Government to which they 
belonged, they received their salaries in silver, but had to make gold 
remittances in support of their families, who were often left behind in England. 
Before 1873, when the price of silver in terms of gold was fixed, this 
circumstance was of no moment to them. But as the rupee began to fall the 
face of the situation was completely altered. With every fall in the value of silver 
they had to pay more rupees out of their fixed salaries to obtain the same 
amount of gold. Some relief was no doubt given to them in the matter of their 
remittances. The Civil Servants were permitted, at a sacrifice to the 



Government, to make their remittances at what was called the Official Rate of 

Exchange. It is true the difference between the market rate and the official rate was not very considerable. None 
the less, it was appreciable enough for the Civil Servants to have gained by 2 1/2 per cent, on the average of the years 

1862-90 at the cost of the Government. The Military Servants obtained a similar 
relief to a greater degree, but in a different way. Their salary was fixed in 
sterling, though payable in rupees. It is true the Royal Warrant which fixed their 
salary also fixed the rate of exchange between the sterling and the rupee for 
that purpose. But as it invariably happened that the rate of exchange fixed by 
the Warrant was higher than the market rate, the Military Servants were 
compensated to the extent of the difference at the cost of the Indian 
Exchequer##. 


##Cf. F. S. 1887-8, pp. 39-40. 
This cost was as follows — 


1 847 -75 

Rs. 6,40,000 

1 885 -86 

Rs. 4,00,000 

1 884 -85 

Rs. 18,43,000 

1886 -87 

Rs. 5,15,000 


This relief was, comparatively speaking, no relief to them. The official or the 
warrant rates of exchange, though better than the market rates of exchange, 
were much lower than the rate at which they were used to make their 
remittances before 1873. Their burden, like that of the Government, grew with 
the fall of silver, and as their burden increased their attitude became alarmist. 
Many were the memorialists who demanded from the Government adequate 
compensation for their losses on exchange. The Government was warnedthat 
"the ignorant folk who think India would be benefited by lowering present 
salaries are seemingly unable to comprehend that such a step would render 
existence on this reduced pay simply impossible, and that recourse would of 
necessity be had to other methods of raising money." 

Such, no doubt, was the case in the earlier days of the East India Company, 
when the Civil Servants fattened on pickings because their pay was small, and 
it was to put a stop to their extortion’s that their salaries were raised to what 
appears an extra-ordinary level. That such former instances of extortion’s 
should have been held out as monitions showed too well how discontented the 
Civil Service was owing to its losses through exchange. 

Quite a different effect the fall had on the trade and industry of the country. It 
was in a flourishing state as compared with the affairs of the Government or 
with the trade and industry of a gold-standard country like England. Throughout 
the period of falling silver there was said to be a progressive decline relatively 
to population in the employment afforded by various trades and industries in 




England. The textile manufactures and the iron and coal trade were depressed 
as well as the other important trades, including the hardware manufactures of 
Birmingham and Sheffield, the sugar-refining of Greenock, Liverpool, and 
London, the manufactures of earthenware, glass, leather, paper, and a 
multitude of minor industries. The depression in English agriculture was so widespread 
that the Commissioners of 1892 were " unable to point to any part of the country in which [the 
effects of the depression] can be said to be entirely absent," and this notwithstanding the fact 
that the seasons since 1882 "were on the whole satisfactory from an agricultural point of view." 
Just the reverse was the case with Indian trade and industry. The foreign trade of the country, 
which had bounced up during the American Civil War, showed greater buoyancy after 1870, 
and continued to grow throughout the period of the falling exchange at a rapid pace. During the 
short space of twenty years the total imports and exports of the country more than doubled in 
their magnitude, as is shown by Table XIV. 

TABLE XIV 


IMPORTS AND EXPORTS (BOTH MERCHANDIZE AND TREASURE) 


Year 

Exports 

Imports 

Year 

Exports 


R. 



R. 

1870-71 

57,556,951 

39,913,942 

1881-82 

83,068,198 

1871-72 

64,685,376 

43,665,663 

1882-83 

84,527,182 

1872-73 

56,548,842 

36,431,210 

1883-84 

89,186,397 

1873-74 

56,910,081 

39,612,362 

1884-85 

85,225,922 

1874-75 

57,984,549 

44,363,160 

1885-86 

84,989,502 

1875-76 

60,291,731 

44,192,378 

1886-87 

90,190,633 

1876-87 

65,043,789 

48,876,751 

1887-88 

92,148,279 

1877-78 

67,433,324 

58,819,644 

1888-89 

98,833,879 

1878-79 

64,919,741 

44,857,343 

1889-90 

105,366,720 

1879-80 

69,247,511 

52,821,398 

1890-91 

102,350,526 

1880-81 

76,021,043 

62,104,984 

1891-92 

111,460,278 


TABLE XV. 

NATURE OF INDUSTRIAL 
PURSUITS IN ENGLAND AND 
INDIA 



Distribution of Indian Exports exclusive of 
Treasure. 

Distribution of English Exports exclusive of 
Treasure. 


Man uf act 

Raw 

Food 

Unclas 

Total. 

Manufact 

Raw 

Food 

Unclassified 

Total. 


ured 

Materials. 

Articles 

sified 


ured 

Materia 

Articles 

Articles. 



Articles. 



Articles 


Articles. 

Is. 




1857 

11 

34 

22 

23 

100 

90.9 

4 

4.9 

.2 

100 

1858 

6 

35 

26 

33 

100 

91.4 

3.4 

5.1 

.1 

100 

1859 

6.5 

40 

15.5 

38 

100 

91.5 

3.8 

4.6 

.1 

100 




I860 

5.7 

43.6 

17.7 

33 

100 

91.9 

3.6 

4.4 

.3 

100 

1861 

5.8 

46.5 

15.3 

32.4 

100 

90.4 

4.8 

4.8 

— 

100 

1862 

5 

52 

16 

27 

100 

90.3 

4 

4.8 

.9 

100 

1863 

3.7 

58.7 

10.6 

27 

100 

91.0 

4 

4 

1.0 

100 

1864 

4 

69.2 

9.3 

17.5 

100 

92.5 

3.7 

3.7 

.1 

100 

1865 

3.5 

68 

12 

16.6 

100 

92.1 

3.6 

3.6 

.7 

100 

1866 

4.2 

67.2 

10.3 

18.3 

100 

92 

3.7 

3.7 

.4 

100 

1867 

4 

58 

11 

27 

100 

92.2 

3.8 

3.7 

.3 

100 

1868 

4 

58-5 

11.5 

26 

100 

92 

4.4 

3.4 

.2 

100 

1869 

4.8 

60.5 

14 

20.7 

100 

92 

4.2 

3.1 

.7 

100 

1870 

4.4 

63.6 

9 

23 

100 

91 

4 

4 

1.0 

100 

1871 

3.7 

65.3 

11 

20 

100 

90 

4-4 

4.9 

.7 

100 

1872 

3-3 

61.4 

13.5 

21.8 

100 

91.2 

5.4 

3.5 

.9 

100 


TABLE XVI 

CHANGES IN INDUSTRIAL PURSUITS OF INDIA 



Imports 

Exports 

Years 

Manufactured 

Raw 

Manufactured 

Raw 


Rs. 

Rs. 

Rs. 

Rs. 

1879 

25,98,65,827 

13,75,55,837 

5,27,80,340 

59,67,27,991 

1892 

36,22,31,872 

26,38,18,431 

16,42,47,566 

85,52,09,499 

Percentage of increase 

39 

91 

211 

43 

Total Annual 

2.8 

6.5 

15 

3 


Not only had the trade of India been increasing, but the nature of her 
industries was also at the same time undergoing a profound change. Prior to 
1870, India and England were, so to say, non-competing groups. Owing to the 
protectionist policy of the Navigation Laws, and owing also to the substitution of 
man by machinery in the field of production, India had become exclusively an 
agricultural and a raw-material-producing country, while England had 
transformed herself into a country which devoted all her energy and her 
resources to the manufacturing of raw materials imported from abroad into 
finished goods. How marked was the contrast in the industrial pursuits in the 
two countries is well revealed by the analysis of their respective exports in 
Table XV. 

After 1870, the distribution of their industrial pursuits was greatly altered, and 
India once again began to assume the role of a manufacturing country. 





Analyzing the figures for Indian imports and exports for the twenty years 
succeeding 1870, (see Table XVI) we find that the progress in the direction of 
manufactures formed one of the most significant features of the period. 

This change in the industrial evolution was marked by the growth of two 
principal manufactures. One of them was the manufacture of cotton. The cotton 
industry was one of the oldest industries of India, but during 100 years between 
1750 and 1850 it had failed into a complete state of decrepitude. Attempts were 
made to resuscitate the industry on a capitalistic basis in the sixties of the 
nineteenth century and soon showed signs of rapid advance. The story of its 
progress is graphically illustrated in the following summary in Table XVII : — 


TABLE XVII 

THE DEVELOPMENT OF INDIA COTTON TRADE AND INDUSTRY 



Growth of Trade (Average Annual Quantities 


in each Quinquennium) 


1870-71 

1 875-76 

1880-81 

1885-86 

1890-91 


to 

to 

to 

to 

to 


1874-75 

1879-80 

1 884-85 

1889-90 

1 894-95 

Imports of raw cotton — 
thousands of cwts. 

23 

52 

51 

74 

89 

Imports of raw cotton — 
thousands of cwts. 

5236 

3988 

5477 

5330 

4660 

Imports of twist and yarn 

33.55 

33.55 

44.34 

49.09 

44.79 


Growth of Industry ( 

[at end of each fifth year) 

Number of mills 

48 

58 

81 

114 

143 

Number of spindles — 000 — 
omitted 

1,000 

1,471 

2,037 

2,935 

3,712 

Number of looms — 000 — 
omitted 

10 

13 

16 

22 

34 

Number of persons 
employed 


39,537 

61,836 

99,224 



Another industry which figured largely in this expansion of Indian 
manufactures was jute. Unlike the cotton industry of India, the jute industry was 
of a comparatively recent origin. Its growth, different from that of the cotton 
industry, was fostered by the application of European capital, European 
management, and European skill, and it soon took as deep roots as the cotton 
industry and flourished as well as the latter did, if not better. Its history was one 



of continued progress as will be seen from Table XVIII. 

This increasing trend towards manufactures was not without its indirect 
effects on the course of Indian agriculture. Prior to 1870 the Indian farmer, it 
may be said, had no commercial outlook. He cultivated not so much for profit as 
for individual self-sufficiency. After 1870 farming tended to become a business 
and crops came more and more to be determined by the course of market 
prices than by the household needs of the farmer. This is well illustrated by 
figures in Table XIX. 

Such was the contrast in the economic conditions prevalent in the two 
countries. This peculiar phenomenon of a silver-standard country steadily 
progressing, and a gold-standard country tending to a standstill, exercised the 
minds of many of its observers. 

Contents Contiuned... 


