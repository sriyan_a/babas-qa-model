REVIEW 

REPORT OF THE TAXATION ENQUIRY COMMITTEE, 1926 

Report of the Taxation Inquiry Committee 1926-1 

COMMISSIONS to report and committees to enquire are a peculiar feature 
of the English system of government. It is a cardinal principle of English 
Parliamentary action that in the matter of social and economic legislation it 
never takes a leap in the dark. Committees and commissions are necessary 
preliminaries of an Act of Parliament. In this it follows the well-known maxim 
that knowledge is power. One is happy to find that this principle of English 
Parliamentary action has been followed in India and our politicians, who so 
often oppose the appointment of Commissions and Committees, cannot be 
said to be acting in the best interests of the country. 

In the case of the Taxation Enquiry Committee, however, it was the 
Government which was trying to shut it out and when it did institute an 
enquiry, it was not the one demanded by the Assembly. What the Assembly 
wanted was an Enquiry into the taxable capacity of the people and this the 
Government did not want to face for fear that such an enquiry might reveal 
that the burden of taxation upon the people was disproportionate to their 
taxable capacity. But when public opinion insisted upon the institution of such 
an enquiry, it, by a species of circumvention, split the enquiry into two parts : 
(1) The taxation Enquiry Committee and (2) The Economic Enquiry 
Committee, with the result that the utility of either committee's report has been 
considerably diminished. 

The terms of reference to the Taxation Enquiry Committee directed it (1) to 
examine the manner in which the burden of taxation is distributed at present 
between the different classes of the population ; (2) to consider whether the 
whole scheme of taxation is equitable and in accordance with economic 
principles, and, if not, in what respects it is defective ; and (3) to report on the 
suitability of alternative sources of taxation. In making its Report, the 
Committee has not been very judicious in the allotment of space to the 
consideration of these three questions. The first was evidently the most 
important of the three heads comprised in the whole charge. Yet the space 
devoted to the consideration of it barely covers 13 pages in a volume of 447 
pages. Besides the treatment of the subject is far from satisfactory. The 
Committee without giving any reason whatsoever divided the population of 
the country in 11 classes and has discussed the burden they bear in 10 
pages and a half without at all touching upon the most important of all 
questions, vis., the incidence of the individual taxes imposed under the Indian 
fiscal system. Now one would have liked to know why did the Committee think 



that 1 1 was an exhaustive classification ? If it is just a question of may be, 
then why not 13 ? Again, how can the Committee at all say what is the burden 
that a merchant bears ? If they had examined the incidence of individual 
taxes, they would have perhaps found that he bore none I Take again, 
another specific instance, that of the Cotton Excise Duty. The Committee has 
no difficulty in saying that its abolition will benefit the working classes. But is 
the Committee quite certain that it was shifted on to the consumer ? I do not 
at all wish to be unfair to the Committee. But I am bound to say that in this 
respect the Report of the Committee is a most disappointing document. The 
Committee has devoted a great deal of space to the detailed history of the 
various sources of taxation in India. So far so good. But it would have been 
far better if the Committee had devoted half of that space in discussing the 
incidence of each tax separately. But this the Committee has entirely omitted 
to do. If that was done, the Committee would have been in a better position to 
deal with the question of the distribution of the burden of taxation and of the 
elimination of the iniquitous taxes. That it has not been able to do as well as 
was to be expected from a Committee which has cost the country nearly Rs. 
4 1/2 lakhs exclusive of printing is due to the fact that it forgot to consider the 
question of incidence, which, after all, was the most important part of its 
enquiry. 

This failure of the Committee to tackle the main problem is to be attributed 
primarily to the personnel of the Committee, which was largely of inexpert 
people, most of whom, if rumour be true, began to learn the A.B.C. of Punjab 
Finance after they found themselves nominated on the Committee. There is 
no wonder if the report emanating from such a body falls flat upon students of 
the subject. One thing, however, can be said in favour of the Report. It is a 
document full of common-sense, neatly arranged. If it can not satisfy the 
student, it will certainly serve as a base for his intellectual operations. Some 
of the proposals of the Committee I hope to examine in subsequent articles. 
For the present I propose to stop with this statement of my view on the 
Report in general. 



