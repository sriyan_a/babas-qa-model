WHAT CONGRESS AND GANDHI HAVE DONE TO THE 

UNTOUCHABLES 


Appendices 

Appendix XIII : Communal Distribution of Population by Minorities in Indian States 

APPENDIX XIII 

COMMUNAL DISIRIBUTION OF POPULATION BY MINORITIES 

IN INDIAN STATES. 


Province 


Muslims 

Scheduled Castes 

Indian Christians 

Sikhs 

States and 
Agencies 

Total 

Population 

Population 

% 

Population 

% 

Population 

% 

Population 

% 

Assam 

725,655 

31,662 

4.4 

265 

.04 

25,913 

3.6 

381 

.05 

Baluchistan 

356,204 

346,251 

97.2 

65 

.02 

40 

.01 

126 

.04 

Baroda 

2,855,010 

223,610 

7.8 

230,794 

8.1 

9,182 

.3 

566 

.02 

Bengal 

2,144,829 

372,113 

17.3 

269,729 

12.6 

564 

.03 

28 

.001 

Central India 

7,506,427 

439,850 

5.9 

1,027,009 

13.7 

7,582 

.1 

2,731 

.04 

Chattiagarh 

4,050,000 

28.773 

0.7 

483,132 

11.9 

11,820 

.3 

507 

.01 

Cochin 

1,422,875 

109,188 

7.7 

141,154 

9.9 

399,394 

28.1 

9 

— 

Deccan (and 
Kolhapur) 

2,785,428 

182,036 

6.5 

306,898 

11.0 

17,236 

.6 

22 

.001 

Gujarat 

1,458,702 

58,000 

3.9 

55,204 

3.8 

4,215 

.3 

182 

.01 

Gwalior 

4,006,159 

240,903 

6.0 

— 

— 

1,352 

.03 

2,342 

.06 

Hyderabad 

16,338,534 

2,097,475 

12.8 

2,928,048 

17.9 

215,989 

1.3 

5,330 

.03 

Kashmere 

and 

Feudatories 

4,021,616 

3,073.540 

76.4 

113,464 

2.8 

3,079 

.08 

65,903 

1.6 

Madras 

498,754 

30,263 

6.0 

83,734 

16.8 

20,806 

4.2 

5 

— 

Mysore 

7,329,140 

485,230 

6.6 

1,405,067 

19.2 

98,580 

1.3 

269 

.004 

N.W.F.P. 

46,267 

22,068 

47.7 

Nil 

— 

571 

1.2 

4,472 

9.1 

Orissa 

3,023,731 

14.355 

0.47 

352,088 

11.6 

2,249 

.07 

151 

.005 

Punjab 

5,503,554 

2,251,459 

40.9 

349,962 

6.4 

6,952 

.1 

1,342,685 

24.4 

Punjab Hill 

1,090,644 

46,678 

4.3 

238,774 

21.9 

188 

.02 

17,739 

1.6 



Rajputana 

13,670,208 

1,297,841 

9.5 

— 

— 

4,349 

.03 

81,896 

.6 

Sikkim 

121,520 

83 

0.07 

76 

.06 

34 

.03 

1 

— 

Travancore 

6,070.018 

434,150 

7.2 

395,952" 

6.5 

1,958,491 

32.3 

31 

— 

U. P. 

928,470 

273,625 

29.5 

152,927 

16.5 

1,281 

.1 

731 

.08 

Western 

India 

4,904,156 

600,440 

12.2 

358,038 

7.3 

3,105 

0.6 

239 

.005 

Total 

90,857,901 

12,659,593 

13.9 

8,892.380 

9.7 

2,792,972 

3.1 

1,526,346 

1.7 


Contents 


Continued... 



