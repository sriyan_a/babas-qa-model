# babas-qa-model

This is my research into making an Question-Answer model, that learns from a repository of Babasaheb Dr. Ambedkar's writings. Long term goal is to use this architecture to train model on all anti-caste works.

## TODO

- [ ] Get data dynamically fron archive instead of storing it.
- [ ] Clean dataframe. Multiple docs give same info.
- [ ] DPR retrival models.
- [ ] Facebook and Microsoft's readers after making sure no None's book name.

## Preprocessing steps:

- [ ] Remove qotations like Gandhi's letter
